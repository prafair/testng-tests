package org.example.allure.testng;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class WebDriverTest {

    private WebDriver driver;

    @BeforeClass
    public void testSetup() {
        System.setProperty("webdriver.chrome.driver", "chromedriver");
        driver = new ChromeDriver();
    }

    @Test
    public void FirstTest() {
        String baseUrl = "https://www.lambdatest.com/";
        String expectedTitle = "LambdaTest - Perform all your tests on cloud";
        String actualTitle = "";
        driver.get(baseUrl);
        actualTitle = driver.getTitle();
        Assert.assertEquals(actualTitle, expectedTitle, "Test Failed");
    }

    @AfterClass
    public void testTeardown() {
        driver.close();
    }

}

